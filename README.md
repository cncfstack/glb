# GLB

GLB (GatewayLoadBalance) 是一款高性能的边界网关及LB设备, 能够为虚拟机、物理机、Kubernetes等提供4/7层的代理及负载均衡及其他功能。

4层代理：
- TCP/UDP 负载均衡
- VIP 管理

7层：
- HTTP 负载均衡
- HTTPS 证书管理
- HTPPS 加密及解密
- API 管理
- 路由管理
- 限流
- 重写
- 灰度发布

GLB 是基于 LVS + Keepalived + Envoy 技术实现，提供 WEB 页面进行配置和管理，能够动态下发配置使配置生效

![GLB](asset/images/GLB.jpg)
